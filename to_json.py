import json
import re
from typing import List
import pandas as pd

data = pd.read_csv(
    '../topic2vec/data_processed/all_transcripts_processed-29-sept.csv',
    index_col=0
)
print(data.head())
print(data.columns)

print(data["sentences"])

all_sentences = []
data['sentences'].apply(
    lambda x: all_sentences.extend(eval(x))
)
print(len(all_sentences))


def process_sentence(sentence: str) -> List[str]:
    """Process a sentence."""
    sentence = re.sub(r'[^\w ]', " ", sentence)
    tokens = re.split(' +', sentence)
    tokens = list(filter(lambda x: len(x) > 0, tokens))
    return tokens


tokens = data['sentences'].apply(process_sentence).tolist()

with open('./data_text.json', "w") as _:
    json.dump(tokens, _)
