const scale = 20;
const border = 30 * scale;
const width = 2000 * scale;
const offset = width / 2;
const textScale = 0.5 * scale;

function generateTree(data) {
  tree = (data) => {
    const root = d3.hierarchy(data);
    root.dx = border;
    root.dy = width / (root.height + 1);
    return d3.tree().nodeSize([root.dx, root.dy])(root);
  };

  const root_left = tree(data["left"]);
  const root_right = tree(data["right"]);

  // Get the heights for each graphics
  let min_left = Infinity;
  let max_left = -min_left;

  root_left.each((d) => {
    if (d.x > max_left) max_left = d.x;
    if (d.x < min_left) min_left = d.x;
  });

  let min_right = Infinity;
  let max_right = -min_right;

  root_right.each((d) => {
    if (d.x > max_right) max_right = d.x;
    if (d.x < min_right) min_right = d.x;
  });

  // Heights to position the graphics perfectly
  let left_height = max_left - min_left;
  let right_height = max_right - min_right;

  let height = Math.max(left_height, right_height) + 2 * border;

  let offset_y_left = left_height / 2 + border * 2;
  let offset_y_right = right_height / 2 + border * 2;

  offset_y_left = offset_y_left + (height - left_height) / 2;
  offset_y_right = offset_y_right + (height - right_height) / 2;

  const offset_x_left = width / 2 + border;
  const offset_x_right = width / 2 + border;

  const svg = d3
    .select("#chart")
    .attr("viewBox", [0, 0, 2 * width, height + 2 * border]);

  svg.selectAll("*").remove();

  // LEFT
  let g = svg
    .append("g")
    .attr("font-family", "sans-serif")
    .attr("transform", `translate(${width / 2},0)`);

  let link = g
    .append("g")
    .attr("fill", "none")
    .attr("stroke", "#555")
    .attr("stroke-opacity", 0.4)
    .attr("stroke-width", 1.5 * scale);

  link
    .selectAll("path")
    .data(root_left.links())
    .join("path")
    .attr(
      "d",
      d3
        .linkHorizontal()
        .x((d) => offset_x_left - d.y)
        .y((d) => offset_y_left + d.x)
    );

  let node = g
    .append("g")
    .attr("stroke-linejoin", "round")
    .attr("stroke-width", 3)
    .selectAll("g")
    .data(root_left.descendants())
    .join("g")
    .attr(
      "transform",
      (d) => `translate(${offset_x_left - d.y},${offset_y_left + d.x})`
    )
    .attr("class", "left_graph");

  node
    .append("circle")
    .attr("fill", (d) => (d.children ? "#555" : "#999"))
    .attr("r", 2.5);

  node
    .filter((d) => {
      return d.data.name != company_name;
    })
    .append("text")
    .attr("font-size", `${textScale}em`)
    .attr("dy", "0.31em")
    .attr("x", (d) => (d.children ? 6 : -6))
    .attr("text-anchor", (d) => (d.children ? "start" : "end"))
    .text((d) => d.data.name)
    .clone(true)
    .lower()
    .attr("stroke", "white");

  // RIGHT

  g = svg
    .append("g")
    .attr("font-family", "sans-serif")
    .attr("transform", `translate(${width / 2},${0})`)
    .attr("fill", "red");

  link = g
    .append("g")
    .attr("fill", "none")
    .attr("stroke", "#555")
    .attr("stroke-opacity", 0.4)
    .attr("stroke-width", 1.5 * scale);

  link
    .selectAll("path")
    .data(root_right.links())
    .join("path")
    .attr(
      "d",
      d3
        .linkHorizontal()
        .x((d) => offset_x_right + d.y)
        .y((d) => offset_y_right + d.x)
    );

  node = g
    .append("g")
    .attr("stroke-linejoin", "round")
    .attr("stroke-width", 3)
    .selectAll("g")
    .data(root_right.descendants())
    .join("g")
    .attr(
      "transform",
      (d) => `translate(${offset_x_right + d.y},${offset_y_right + d.x})`
    )
    .attr("class", "right_graph");

  node
    .append("circle")
    .attr("fill", (d) => (d.children ? "#555" : "#999"))
    .attr("r", 2.5);

  node
    .filter((d) => {
      return d.data.name != "netflix";
    })
    .append("text")
    .attr("font-size", `${textScale}em`)
    .attr("dy", "0.31em")
    .attr("x", (d) => (d.children ? -6 : 6))
    .attr("text-anchor", (d) => (d.children ? "end" : "start"))
    .text((d) => d.data.name)
    .clone(true)
    .lower()
    .attr("stroke", "white");

  svg
    .append("g")
    .attr("fill", "red")
    .attr("font-size", "40em")
    .attr("text-anchor", "middle")
    .attr("transform", `translate(${width + border}, ${height / 2 + border})`)
    .append("text")
    .text(company_name)
    .attr("class", "company_name");

  remove_center_element();
}

function remove_center_element() {
  for (let class_name of ["right_graph", "left_graph"]) {
    for (let ele of document.getElementsByClassName(class_name)) {
      text = ele.getElementsByTagName("text")[0].innerHTML;
      if (text == company_name) {
        ele.style.display = "none";
      }
    }
  }
}
