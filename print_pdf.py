from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
from time import sleep


def print_pdf(company_code):
    """Print the webpage to pdf."""

    index_file = "file://"+os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "index.html"
    )
    profile = webdriver.FirefoxOptions()
    profile.headless = True
    profile.preferences.setdefault("print.always_print_silent", True)

    profile.set_preference('print_printer', 'Print to File')
    profile.set_preference(
        'print.print_to_filename',
        os.path.join(os.path.join(os.path.dirname(
            os.path.abspath(__file__)), f"print-{company_code}.pdf"))
    )

    driver = webdriver.Firefox(options=profile)
    driver.get(index_file)

    sleep(3)
    driver.execute_script('window.print();')
    # driver.save_screenshot("./image.png")
    print("Printing ...")
    sleep(10)

    driver.close()


if __name__ == "__main__":
    print_pdf()
