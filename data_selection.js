// Data selection
const max_nbr_words = 8;
let raw_data = [];
let company_name = "";

fetch("data_text.json")
  .then((result) => {
    return result.json();
  })
  .then((result) => {
    raw_data = result;
    document.getElementById("search").hidden = false;
  });

function filter_sentences(name) {
  console.log("Filtering with", name);
  let left_part = [];
  let right_part = [];
  company_name = name;
  name = name.toLowerCase();

  raw_data.forEach((sentence) => {
    let new_sentence = sentence.map((x) => x.toLowerCase());
    let index_element = new_sentence.indexOf(name);
    if (index_element >= 0) {
      // Cut the sentences to get only a maximum number of words
      // LEFT
      left_sentence = new_sentence
        .slice(0, index_element)
        .reverse()
        .slice(0, max_nbr_words);
      left_part.push(left_sentence);
      // RIGHT
      right_sentence = new_sentence
        .slice(index_element + 1)
        .slice(0, max_nbr_words);
      right_part.push(right_sentence);
    }
  });
  console.log("Generate the LEFT Tree\n\n");
  console.log(left_part);
  left_tree = generateRightTree(left_part, true);
  console.log("Generate the RIGHT Tree\n\n");
  console.log(right_part);
  right_tree = generateRightTree(right_part, false);
  data = {
    left: {
      name: company_name,
      children: left_tree,
    },
    right: {
      name: company_name,
      children: right_tree,
    },
  };

  generateTree(data);
}

function generateRightTree(sentences, left) {
  // Search unique childrens
  let unique_childrens = {};
  sentences.forEach((sentence) => {
    if (sentence.length > 0) {
      if (Object.keys(unique_childrens).includes(sentence[0])) {
        unique_childrens[sentence[0]].push(sentence.slice(1));
      } else {
        unique_childrens[sentence[0]] = [sentence.slice(1)];
      }
    }
  });

  let tree = [];
  for (let [key, value] of Object.entries(unique_childrens)) {
    value = value.filter((x) => x.length > 0);
    if (value.length == 1) {
      if (left) {
        text = value[0].reverse().join(" ") + " " + key;
      } else {
        text = key + " " + value[0].join(" ");
      }

      tree.push({
        name: text,
      });
    } else if (value.length == 0) {
      tree.push({
        name: key,
      });
    } else {
      childs = generateRightTree(value, left);

      if (childs.length == 1 && childs[0].children == undefined) {
        tree.push({
          name: key + " " + childs[0].name,
        });
      } else if (childs.length == 1 && childs[0].children.length == 1 && true) {
        tree.push({
          name: key + " " + childs[0].name,
          children: childs[0].children,
        });
      } else {
        tree.push({
          name: key,
          children: childs,
        });
      }
    }
  }

  return tree;
}

/*
Format
{
    "name": "name",
    "children": [
        SAME
    ]
}
*/
