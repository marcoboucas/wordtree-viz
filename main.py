#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import re
import json
from pprint import pprint

from print_pdf import print_pdf

model_version = "29-sept"
FILE_LINK = f'../topic2vec/data_processed/all_transcripts_processed-{model_version}.csv'
COL_CODE = "code"
COL_SENTENCES = "sentences"

COMPANY_CODE = "LIZI"
COMPANY_NAME = "lizhi"


# Load the data
data = pd.read_csv(
    FILE_LINK,
    index_col=0
)


# Select only company sentences
sub_df = data[(data[COL_CODE] == COMPANY_CODE)].copy().reset_index()

# Filter if the company name appears
selected_sentences = []
for sentences in sub_df[COL_SENTENCES]:
    for sentence in eval(sentences):
        _ = re.split('\W', sentence.lower())
        _ = list(filter(lambda x: len(x) > 0, _))
        if COMPANY_NAME.lower() in _:
            selected_sentences.append(_)

# Create sentences for left and right parts
left_part, right_part = [], []
for sentence in selected_sentences:
    idx = sentence.index(COMPANY_NAME)
    left, right = sentence[:idx], sentence[idx+1:]
    if len(left) > 0:
        left_part.append(left)
    if len(right) > 0:
        right_part.append(right)

# generate the tree


def build_tree(word, sentences, left: bool = True):
    """build the tree (recursive)."""
    sentences = list(filter(lambda x: len(x) > 0, sentences))

    first_and_last = {}
    for sentence in sentences:
        first, last = sentence[0], sentence[1:]
        if len(last) > 0:
            try:
                first_and_last[first].append(last)
            except:
                first_and_last[first] = [last]
    children = []
    for key, value in first_and_last.items():
        if len(value) == 1:
            if left:
                children.append({
                    "name": " ".join(value[0]+[key])
                })
            else:
                children.append({
                    "name": " ".join([key]+value[0])
                })
        else:
            children.append(build_tree(key, value, left))

    return {
        "name": word,
        "children": children
    }


# Generate Left and Right parts and save them in a json file

#left_part_inverted = list(map(lambda x: list(reversed(x)), left_part))


#left_part_inverted = list(map(lambda x: list(reversed(x)), left_part))
with open('./data.js', "w") as _:
    text = "let data = "+json.dumps({
        "left": build_tree(COMPANY_NAME, left_part, left=True),
        "right": build_tree(COMPANY_NAME, right_part, left=False)
    })+f";\nlet company_name='{COMPANY_NAME}';"
    _.write(text)

# print_pdf(COMPANY_CODE)
