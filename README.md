# WordTree Generation from Earnings Call Transcripts

**This implementation uses the D3.js library and plain javascript.**

Example available here: http://marco.boucas.free.fr/73strings_project/wordtree/
Please wait the textbar to show (after loading the data, that can take a few seconds depending on your connection), you can then write a keyword (for instance `netflix`) and it will generate a wordtree for this word (few seconds again, depending on the task).

Script to generate the WordTree from a text.

Input:

- A list of sentences
  - Each sentence is represented by a list of tokens
- A word (the keyword focused)

Output:

- The tree visualization with the word in the middle

## How to test it ?

Please live-run this folder, by using _live-server_ or something like that (VSCode extension **Live Server**).
Then you will wait a bit for the data to be loaded, and then a textbar will appear.
Write down the keyword (for instance `netflix`) and click on the button (pushing enter will not work sometimes)
The tree will be generated shortly.

## THIS IS A TEST

This repository is a test of wordtree, the code is not perfect (I am not a jaavscript developper) and does not use Angular or some framework.
You just need the d3js library to run it (and sub-libraries provided in the libraries folder)
